#!/usr/bin/env python
import os
import jinja2
import webapp2
import urllib2
import sys
import json
import logging

from google.appengine.ext import db
from google.appengine.api import memcache

template_dir = os.path.join(os.path.dirname(__file__),'template')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)

class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
         self.write(self.render_str(template, **kw))

class Art(db.Model):
    title = db.StringProperty(required = True)
    art = db.TextProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)
    coords = db.GeoPtProperty()


#funcao para debug no servidor, console(string)
def console(s):
    sys.stderr.write(s)


#--------------------------------------------------------------------
GMAPS_URL =  "http://maps.googleapis.com/maps/api/staticmap?size=650x300&maptype=roadmap&sensor=false&"

def gmaps_img(points):
  markers = '&'.join('markers=%s,%s' % (p.lat,  p.lon) for p in points)
  return GMAPS_URL + markers

def get_coords(ip):

    url = "http://freegeoip.net/json/" + str(ip)
    content = None
    try:
        content = urllib2.urlopen(url).read()
    except urllib2.URLError:
        return None
    if content:
        ip_json = json.loads(content)

        return db.GeoPt(ip_json['latitude'], ip_json['longitude'])


def top_arts(update = False):

    key = 'top'
    arts = memcache.get(key)

    if arts is None or update:
        logging.error('erro db')
        arts = db.GqlQuery("SELECT * FROM Art "
                            "ORDER BY created DESC")

        arts = list(arts)
        memcache.set(key, arts)
    return arts


class MainHandler(Handler):
    def render_front(self, title="", art="", error="", created="", ):

        arts = top_arts()


        points = []
        for a in arts:
            if a.coords:
                points.append(a.coords)

        #find which arts have coords
        img_url = None
        points = filter(None, (a.coords for a in arts))
        if points:
            img_url = gmaps_img(points)

        logging.info(img_url)
        self.render("front.html", title=title, art=art, error=error, arts=arts, img_url=img_url)

    def get(self):
        self.render_front()

    def post(self):
        title = self.request.get('title')
        art = self.request.get('art')

        if title and art:
            a = Art(title= title, art= art)
            coords = get_coords(self.request.remote_addr)
            if coords:
                a.coords = coords
            a.put()
            top_arts(True)
            self.redirect("/")

        else:
            error = "precisa de titulo e arte!"
            self.render_front(title, art, error)



app = webapp2.WSGIApplication([
    ('/', MainHandler)
], debug=True)
